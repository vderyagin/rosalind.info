#[derive(Debug, Clone, PartialEq)]
pub enum Nucleotide { A, C, G, T }

impl Nucleotide {
  pub fn complement(&self) -> Self {
    match self {
      &Nucleotide::A => Nucleotide::T,
      &Nucleotide::T => Nucleotide::A,
      &Nucleotide::C => Nucleotide::G,
      &Nucleotide::G => Nucleotide::C,
    }
  }
}

#[derive(Debug, PartialEq)]
pub struct DNA(Vec<Nucleotide>);

impl DNA {
  pub fn new(ns: &[Nucleotide]) -> Self {
    DNA(ns.to_vec())
  }

  pub fn nucleotides(&self) -> &[Nucleotide] {
    let &DNA(ref ns) = self;
    ns
  }
}

impl<'a> From<&'a [u8]> for DNA {
  fn from(raw_dna: &[u8]) -> Self {
    let nucleotides = raw_dna
      .iter()
      .filter(|&byte| b"ACGT".contains(byte))
      .map(|byte| match byte {
        &b'A' => Nucleotide::A,
        &b'C' => Nucleotide::C,
        &b'G' => Nucleotide::G,
        &b'T' => Nucleotide::T,
        _ => panic!("unreachable"),
      })
      .collect();

      DNA(nucleotides)
  }
}
