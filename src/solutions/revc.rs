// Complementing a Strand of DNA
// http://rosalind.info/problems/revc

use dna::{DNA, Nucleotide};

pub fn reverse_complement(dna: DNA) -> DNA {
  let nucleotides: Vec<Nucleotide> = dna
    .nucleotides()
    .iter()
    .rev()
    .map(|nucleotide| nucleotide.complement())
    .collect();

  DNA::new(&nucleotides)
}

#[cfg(test)]
mod tests {
  use dna::DNA;
  use super::reverse_complement;

  const DATASET: &'static [u8] = include_bytes!("rosalind_revc.txt");

  const REV_COMPL: &'static [u8] = b"AATCCACACTCGTCACGTGCTCCCGTCTACGGTAAATTTAAGTAACTCAGTCATTAGCTCTTCGACTAGACCCACTGTCCTGCGCAAGAATAAGGCAAAGTCAACCACCCACGGACTGACAGACGCTATTACTCGTGCAACGGAGAGCAATCCCATATTAGAATATTTTGACCATACACTGGTCGCCAACAGCCGCCCGCCGATAGGAGCCTTCGGTCGTTTCTACTCTGCTCAACTCGCTCGTGCGCCATTAACAGTCCGACTACACTCCTTGGAATGGACCCCACCATAATAAACTAGCACTAACGATCTAAACAATTATGAAATGGACGGTGATCTCCTGCCAAAGAAGCGTCCCGTCGCTGTGTGGGACGGCATGAGCGAGGGTTGGTAAACCACCCTTCCTCCCCGAATGGCACCTTCCACAGCTTCGATTGGGTTTTTTTAAGAACCGAACACCTGTCCAGCATGCATCCAATTGACCTCTGTACAGCGGCTCGAGGTCTAGCGGGTGTTCAGGCTCTCAATTCGTCGTACATGAACGAAAACGCGAGAGGATACTCACGTAATTACTTGGGGCCCAACTCATAGCTAGTGCACAATTCCTAATCATCAGGTTCAGCGACCCCGTCGCGCACTCCCTGTGCACGGTCATGATACTCAGGTTTCCGCTTGAGCTTCCCAAGATACAGGCTCAAGTACCCTGACCTAAAGGAAATTTGTTATCACATTTTGCCTAAAAGGCGCGGAAGACCGTGATAAGAAGGCGTTGAGCCTGGCCGCGCGTATTTTTTGGAGGTGACAGATGATTGAGGAAACCATCTATGCACAGCGATCGGGATAGGATCCTCGCATGAAAGACTAGATACCCATACTAGGCAGCCTTCGTA";

  #[test]
  fn works_for_sample_dataset() {
    assert_eq!(reverse_complement(DNA::from("AAAACCCGGT".as_bytes())),
               DNA::from("ACCGGGTTTT".as_bytes()));
  }

  #[test]
  fn solves_the_problem() {
    assert_eq!(reverse_complement(DNA::from(DATASET)), DNA::from(REV_COMPL));
  }
}
