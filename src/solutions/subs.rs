// Finding a Motif in DNA
// http://rosalind.info/problems/subs

pub fn locations(dna: &[u8], subdna: &[u8]) -> Vec<usize> {
  dna
    .windows(subdna.len())
    .enumerate()
    .filter(|&(_, win)| win == subdna)
    .map(|(idx, _)| idx + 1)
    .collect()
}

#[cfg(test)]
mod tests {
  use std::str;
  use super::locations;

  const DATASET: &'static [u8] = include_bytes!("rosalind_subs.txt");

  #[test]
  fn works_for_sample_dataset() {
    assert_eq!(locations(b"GATATATGCATATACTT", b"ATAT"), vec![2, 4, 10]);
  }

  #[test]
  fn solves_the_problem() {
    let dnas = str::from_utf8(DATASET).unwrap().lines().map(|l| l.trim()).collect::<Vec<_>>();
    assert_eq!(locations(dnas[0].as_bytes(), dnas[1].as_bytes()),
               vec![58, 136, 172, 358, 421, 477, 495, 584, 600, 663, 711, 718, 725, 784, 791, 806, 813]);
  }
}
