// Rabbits and Recurrence Relations
// http://rosalind.info/problems/fib

fn rabbit_pairs(pairs_total: usize, pairs_reproductive: usize, months: usize, litter_size: usize) -> usize {
  if months == 1 { return pairs_total }
  let new_pairs = pairs_reproductive * litter_size;
  rabbit_pairs(pairs_total + new_pairs, pairs_total, months - 1, litter_size)
}

pub fn rabbit_pairs_from_scratch(months: usize, litter_size: usize) -> usize {
  rabbit_pairs(1, 0, months, litter_size)
}


#[cfg(test)]
mod tests {
  use super::rabbit_pairs_from_scratch;

  #[test]
  fn works_for_sample_dataset() {
    assert_eq!(rabbit_pairs_from_scratch(5, 3), 19);
  }

  #[test]
  fn solves_the_problem() {
    assert_eq!(rabbit_pairs_from_scratch(35, 3), 1323839213083);
  }
}
