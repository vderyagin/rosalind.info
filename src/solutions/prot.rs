// Translating RNA into Protein
// http://rosalind.info/problems/prot

use std::collections::HashMap;
use std::str;

fn rna_codon_table() -> HashMap<&'static str, &'static str> {
  vec![("UUU", "F"), ("UUC", "F"), ("UUA", "L"), ("UUG", "L"), ("UCU", "S"),
       ("UCC", "S"), ("UCA", "S"), ("UCG", "S"), ("UAU", "Y"), ("UAC", "Y"),
       ("UAA", "Stop"), ("UAG", "Stop"), ("UGU", "C"), ("UGC", "C"), ("UGA", "Stop"),
       ("UGG", "W"), ("CUU", "L"), ("CUC", "L"), ("CUA", "L"), ("CUG", "L"),
       ("CCU", "P"), ("CCC", "P"), ("CCA", "P"), ("CCG", "P"), ("CAU", "H"),
       ("CAC", "H"), ("CAA", "Q"), ("CAG", "Q"), ("CGU", "R"), ("CGC", "R"),
       ("CGA", "R"), ("CGG", "R"), ("AUU", "I"), ("AUC", "I"), ("AUA", "I"),
       ("AUG", "M"), ("ACU", "T"), ("ACC", "T"), ("ACA", "T"), ("ACG", "T"),
       ("AAU", "N"), ("AAC", "N"), ("AAA", "K"), ("AAG", "K"), ("AGU", "S"),
       ("AGC", "S"), ("AGA", "R"), ("AGG", "R"), ("GUU", "V"), ("GUC", "V"),
       ("GUA", "V"), ("GUG", "V"), ("GCU", "A"), ("GCC", "A"), ("GCA", "A"),
       ("GCG", "A"), ("GAU", "D"), ("GAC", "D"), ("GAA", "E"), ("GAG", "E"),
       ("GGU", "G"), ("GGC", "G"), ("GGA", "G"), ("GGG", "G")].into_iter().collect()
}

pub fn rna_to_protein(rna: &[u8]) -> Vec<u8> {
  let codons = rna_codon_table();

  rna
    .chunks(3)
    .map(|chunk| str::from_utf8(chunk).unwrap())
    .map(|codon| codons.get(&codon).unwrap())
    .take_while(|&&acid| acid != "Stop")
    .map(|acid| acid.as_bytes()[0])
    .collect()
}


#[cfg(test)]
mod tests {
  use super::rna_to_protein;
  use std::str;

  const DATASET: &'static [u8] = include_bytes!("rosalind_prot.txt");

  const PROTEIN: &'static [u8] = b"MASFHLRQASQKCLKTPGLGCRFVSGGRAHGMTKSKCTNLAKTEITTWFSSESQTGGITQWLEILMLSAPVLSGSLILTCSVTQTGSLIFVVPLIRMRGKNIFKIVPMPVVPDVSGWRVGTPSPRNRHTLSVLRRRQRRHPHADPGGAPTHSFDDYGSTIMIRHCIAFRTEELPDYCKVKDRSPLYVHLRGIVARNNTYHDWAHGKPDISPVIAYGQKVLLVTFRCYLPKLRFGAEPGKVFLTPNSACFEGCHRGNYLSIAFSAKTLCPTCVMANLPQRATNSNPTTGSRFRGLKTTPCSSNTGIPPVDWLAGSAPQCMHPTSMRQHRIARATYITRETVVAQVGLPVIRPWPSFGWSGQSFGEAYSVLMACGPAYAWAKQVYIRLWDDVKTWEAILPLSSPMAGQPSVDCLSNIIVAQLIVRSQYYPCLPRCRLFGTFNRRCHKSSIVHFKIVSECKRGCKQTEFEQFRIDVQREGNWCSSGDAPTRSPQSLCRFDSMSGPRRLALLSDHGSPITRGRAAGMGCPSEQEFPRSGACRRVTYVGSPDSVGLINRVDGVHRDNTVSWVLIDIESCRTYNVYGVPALYEIMRSLGPEVDIRTTRLTFRWLPGYFKMRYFSVSALSGLRPIARYTLHQSAELRKGALSAKVCMKVPLTSSPDEHGYPLGLRLKKPHGSTRDQHSPVTCAASRVRYSCEWSPHPGLVGLLCEAHRVENLATLTSLIALPSTSWPPMISRCHGPRQYLLIITSKRPRRKYGFLGGNMSYRYGLRHSHMRFPNAMDGHIRIRTPLWRFKCSTVEVISDHISSGSFPGLRDAPLSTYPCLGCLAVRKNSDKARTPMTLNRWPGIRYLAKILSSATTFRSLRLASPVRALERRITIKAVLNLHCFQENVHAAFSFPYSVGLLHIEPVVLLYSDSGRGTRCFEDSLSGDSYRSSSTCARRFCDLRGNWTGVGGLLISSAHLPPHWTLKVILGAMSSSQTSQETLLISLKHDYGVEVTGSNGSESIVQLNTGTASHPARSIKDLLYIKKLAVSAPHPERPLPHRVSNNNPETDRPDVATTANCSLERQGSQTALSNLVLTGNKIHLYSLPPIPDCEYREPCSTTSLQRNISMFLCRTIGRIVNLRCDLIEDAFTPLTYVPCSSECESVGCHPTQPIAGLRFNPNYKVAFPRGVLSGSSTSVTHITRCTCMSAPSVRNLSGINMVALLYLRSILRPLRDQRSIPHPKAMYTVTIPLRDVKARRPQVLKPRFMLRKHFLSNIIRPVPWAGLERRLEVVLLLTLVWFRLMYITSVVKRIRMQTTACIVIAGPVKEFVKRLAFSCPSELAAFFFNRDETTACLVRERGGLDLDACLFTGLGVAGYPHGRLWRLSLENLRFDHARRILADLCVDEMIVRGFMAPNRQRMGTWRIPPCVGIRYLQSPWSRLNQHPLLSPLTVDSLLFTLSQARRYMGTHTVPPLCYCWSNGFNTLSVQPVVLLIGVLPEVWTTTVVQLSRSGPEHRCLSFIQNSSWSDIHRSPLVSNDTANPRLVTHSFCILMVIPTVIGLCPLISIDRILSRIGLVLCGSWSRGLRTNCRVDCYLLCALRRTMPGKILVLRRTLMHNRRSRAIERAWWRSRHIVQINDLTLVTHIVETERKAKPIVFRQLERLIRMLRQSGFEIYTITSGTFQSNTVSARLRALLTRSNELLLRASINTCLTSVRRKERLPAYFRHGDRKTSLYGRTGEIRFAKINRATSGGTYRRPQGKCTAVRIVFRKTRVRQCDKNRAVYLRVSMPRARGFPFCKSDLIYGMWIRYYLPQGGSLTHNVNEIAPVGVKLHGILNAAKIPDPVLPRARTTFTFLPFIMSEEFPLVPACIHILFTIHDRVELQLTQYAVLKRLVHHKPMVDKTRDCATRIDLFDFAELVVRKQGKLQRVRRSVLVDLGAPHHTQEKHEQVTLIHFCHIPMMRQCERQIPTVTTSSPLRHGGTLLDPRTLFSQVDNYRICYRIRNVVRTSTELPSTINTEFSVPMDFLLLLSWSPLSLHKSSDPCVPWRLHVLRHAHVCTRDKEIPTSDKTGWTRWPLNRIKDNGYGSRASRTSLRKIYSPNSIPFESIYYEALSPRHIRSSRGAKDSQVDVTFLLNFSVRRRGSQFGYALSNSPLAGFRIKARQGQSDFFFTLAPGAGRVTTRTSKSRRVSTAFLTSCACRLILFVDGGYMLLIANHAINSTPYTMDRGLQSRIYTQNCIATLAAAIQIEVTLHILARTPVRLIQMAGAELETYLAMHPNPKALRIIHLSLAATISRLLLFNHVLQMQPDLFGDSSPPKNLLICEITESLLLLCTDVLDRAPDVSDVGRSLRMRTLQGWNMKYISGRVSTKMLLLSGLSLVSSLAWDYHTLLTWWEAATRQIHVISEYKIAGIRHVSVHNGNRLTATANSCVCRDLFCIRNTTRTQLTSEGFRWVHHTSHCRLQHFPPAVELLYTILPLSLSRRGWGLNEVGVTETNGVAALLTEIELGHHGPLEGGGLLRYRIMAPVNTTYNCNCPKPADDSTYSSVFTTRGKPVPRQLFGCPGTSYGALSCLRAFSQISNWAFTASIHFTLMVVNWLITSAQTSSRIPHTCREHARGWCYLLLKHGCIQSADLYSASGEANSHTLLDLTLKSCVQVVCQVRELCVHNYEPKRGGTHSLIAVNQS";

  #[test]
  fn works_for_sample_dataset() {
    let rna = b"AUGGCCAUGGCGCCCAGAACUGAGAUCAAUAGUACCCGUAUUAACGGGUGA";
    let protein = b"MAMAPRTEINSTRING";
    assert_eq!(&rna_to_protein(rna), protein);
  }

  #[test]
  fn solves_the_problem() {
    assert_eq!(rna_to_protein(DATASET), PROTEIN);
  }
}
