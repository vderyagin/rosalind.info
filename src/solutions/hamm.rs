// Counting Point Mutations
// http://rosalind.info/problems/hamm

pub fn hamming_distance(seq1: &[u8], seq2: &[u8]) -> usize {
  seq1.iter()
    .zip(seq2)
    .filter(|&(a, b)| a != b)
    .count()
}

#[cfg(test)]
mod tests {
  use std::str;
  use super::hamming_distance;

  const DATASET: &'static [u8] = include_bytes!("rosalind_hamm.txt");

  #[test]
  fn works_for_sample_dataset() {
    assert_eq!(hamming_distance(b"GAGCCTACTAACGGGAT", b"CATCGTAATGACGGCCT"),
               7);
  }

  #[test]
  fn solves_the_problem() {
    let dnas = str::from_utf8(DATASET).unwrap().lines().map(|l| l.trim()).collect::<Vec<_>>();
    assert_eq!(hamming_distance(dnas[0].as_bytes(), dnas[1].as_bytes()),
               508);
  }
}
