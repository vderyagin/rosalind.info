// Computing GC Content
// http://rosalind.info/problems/gc

use std::collections::HashMap;
use num_rational::Ratio;

pub fn ratio_to_float(r: Ratio<usize>) -> f64 {
  *r.numer() as f64 / *r.denom() as f64
}

pub fn gc_content(dna: &str) -> Ratio<usize> {
  let total = dna.chars().count();
  let gc = dna.chars().filter(|ch| *ch == 'G' || *ch == 'C').count();

  Ratio::new(gc * 100, total)
}

pub fn parse_fasta(fasta: &str) -> HashMap<String, String> {
  let mut map = HashMap::new();
  let mut key = "";
  let mut dna = String::new();

  for line in fasta.lines() {
    if line.starts_with('>') {
      if key != "" { map.insert(key.to_string(), dna.to_string()); };
      key = line.trim().trim_left_matches('>');
      dna = String::new();
    } else {
      dna.push_str(line.trim());
    }
  }

  map.insert(key.to_string(), dna.to_string());

  map
}

#[cfg(test)]
mod tests {
  use std::str;
  use super::{parse_fasta, ratio_to_float, gc_content};

  const SAMPLE_INPUT: &'static str =
    ">Rosalind_6404
CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC
TCCCACTAATAATTCTGAGG
>Rosalind_5959
CCATCGGTAGCGCATCCTTAGTCCAATTAAGTCCCTATCCAGGCGCTCCGCCGAAGGTCT
ATATCCATTTGTCAGCAGACACGC
>Rosalind_0808
CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGACCAGCCCGGAC
TGGGAACCTGCGGGCAGTAGGTGGAAT";

  const DATASET: &'static [u8] = include_bytes!("rosalind_gc.txt");

  #[test]
  fn works_for_sample_dataset() {
    let map = parse_fasta(SAMPLE_INPUT);
    let res = map.iter().max_by_key(|&(_, v)| gc_content(v.as_str()));

    assert!(res.is_some());
    assert_eq!("Rosalind_0808", res.unwrap().0);
    assert!((ratio_to_float(gc_content(res.unwrap().1)) - 60.919540).abs() < 0.001);
  }

  #[test]
  fn solves_the_problem() {
    let map = parse_fasta(str::from_utf8(DATASET).unwrap());
    let res = map.iter().max_by_key(|&(_, v)| gc_content(v.as_str()));

    assert!(res.is_some());
    assert_eq!("Rosalind_1269", res.unwrap().0);
    assert!((ratio_to_float(gc_content(res.unwrap().1)) - 52.11267).abs() < 0.001);
  }
}
