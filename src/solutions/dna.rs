// Counting DNA Nucleotides
// http://rosalind.info/problems/dna

use dna::{DNA, Nucleotide};

fn count_nucleotides(dna: DNA) -> (usize, usize, usize, usize) {
  dna
    .nucleotides()
    .iter()
    .fold((0, 0, 0, 0), |freqs, nucleotide| {
      let (a, c, g, t) = freqs;
      match nucleotide {
        &Nucleotide::A => (a + 1, c, g, t),
        &Nucleotide::C => (a, c + 1, g, t),
        &Nucleotide::G => (a, c, g + 1, t),
        &Nucleotide::T => (a, c, g, t + 1),
      }
    })
}

pub fn frequencies(dna: DNA) -> String {
  let (a, c, g, t) = count_nucleotides(dna);
  format!("{} {} {} {}", a, c, g, t)
}

#[cfg(test)]
mod tests {
  use dna::DNA;
  use super::frequencies;

  const SAMPLE_DNA: &'static [u8] = b"AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC";

  const DATASET: &'static [u8] = include_bytes!("rosalind_dna.txt");

  #[test]
  fn works_for_sample_dataset() {
    assert_eq!(frequencies(DNA::from(SAMPLE_DNA)), "20 12 17 21");
  }

  #[test]
  fn solves_the_problem() {
    assert_eq!(frequencies(DNA::from(DATASET)), "204 234 200 218");
  }
}
