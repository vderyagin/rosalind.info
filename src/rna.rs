#[derive(Debug, Clone, PartialEq)]
pub enum Nucleotide { A, C, G, U }

impl Nucleotide {
  pub fn complement(&self) -> Self {
    match self {
      &Nucleotide::A => Nucleotide::U,
      &Nucleotide::U => Nucleotide::A,
      &Nucleotide::C => Nucleotide::G,
      &Nucleotide::G => Nucleotide::C,
    }
  }
}

#[derive(Debug, PartialEq)]
pub struct RNA(Vec<Nucleotide>);

impl RNA {
  pub fn new(ns: &[Nucleotide]) -> Self {
    RNA(ns.to_vec())
  }

  pub fn nucleotides(&self) -> &[Nucleotide] {
    let &RNA(ref ns) = self;
    ns
  }
}

impl<'a> From<&'a [u8]> for RNA {
  fn from(raw_rna: &[u8]) -> Self {
    let nucleotides = raw_rna
      .iter()
      .filter(|&byte| b"ACGU".contains(byte))
      .map(|byte| match byte {
        &b'A' => Nucleotide::A,
        &b'C' => Nucleotide::C,
        &b'G' => Nucleotide::G,
        &b'U' => Nucleotide::U,
        _ => panic!("unreachable"),
      })
      .collect();

      RNA(nucleotides)
  }
}
